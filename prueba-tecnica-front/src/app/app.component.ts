import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from './services/user.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'prueba-tecnica-front';

  public users
  public filter
  public selectedUser: any
  public edit
  public rol = {
    'AUX': 'Auxiliar',
    'ADM': 'Administrador',
    'AUD': 'Auditor',
  }
  public userForm = new FormGroup({
    name: new FormControl('', Validators.required),
    rol: new FormControl('', Validators.required),
    active: new FormControl(false, Validators.required),
  })

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsers().subscribe({
      next: data => {
        this.users = data['data']
      },
      error: err => {
        console.log('Error')
      }
    })
  }

  createUser(data) {
    this.userService.postUser(data).subscribe({
      next: () => {
        this.searchUser()
        this.userForm.reset()
        this.edit = false
      }
    })
  }

  patchUser(data, pk) {
    this.userService.patchUser(data, pk).subscribe({
      next: () => {
        this.searchUser()
        this.userForm.reset()
        this.edit = false
      }
    })
  }

  loadUser(user) {
    this.edit = true
    this.selectedUser = user
    this.userForm.patchValue(user)
    this.userForm.get('rol').patchValue(user.rol.name)
  }

  unselectUser() {
    this.edit = false
    this.selectedUser = null
    this.userForm.reset()
  }

  searchUser() {
    this.userService.getUsers(this.filter).subscribe({
      next: data => {
        this.users = data['data']
      },
      error: err => {
        console.log('Error')
      }
    })
  }

  deleteUser(pk) {
    this.userService.deleteUser(pk).subscribe({
      next: () => {
        this.searchUser()
        this.unselectUser()
      },
      error: err => {
        console.log(err)
      }
    })
  }
}

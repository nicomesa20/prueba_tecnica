import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public url_api = 'http://localhost:8000/'

  constructor(
    private http: HttpClient
  ) { }

  getUsers(filter?) {
    if (filter) return this.http.get(this.url_api + `api/user?name=${filter}`)
    return this.http.get(this.url_api + 'api/user')
  }

  postUser(body) {
    return this.http.post(this.url_api + 'api/user', body)
  }

  patchUser(body, pk) {
    return this.http.patch(this.url_api + `api/user/${pk}`, body)
  }

  deleteUser(pk) {
    return this.http.delete(this.url_api + `api/user/${pk}`)
  }
}

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from ..models import Rol


class RolScriptView(APIView):
    ''' Script that creates 3 roles '''

    def post(self, request):

        Rol.objects.bulk_create([
            Rol(name='AUX'),
            Rol(name='ADM'),
            Rol(name='AUD')
        ])

        return Response({
            'inserted': 'Roles creados'
        }, status=status.HTTP_201_CREATED)

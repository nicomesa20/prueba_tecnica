from cerberus import Validator
from rest_framework.response import Response
from rest_framework import status
from cerberus import Validator
from rest_framework.views import APIView

from ..models import User, Rol
from ..serializers.user import UserSerializer


class SpecificUserApi(APIView):
    """ Defines the HTTP verbs to user model management. """

    def patch(self, request, pk):
        ''' Updates user '''

        validator = Validator({
            'name': {
                'required': False,
                'type': 'string'
            },
            'rol': {
                'required': False,
                'type': 'string',
                'allowed': ['AUX', 'ADM', 'AUD']
            },
            'active': {
                'required': False,
                'type': 'boolean'
            }
        })
        if not validator.validate(request.data):
            return Response({
                "code": "invalid_body",
                "detailed": "Cuerpo con estructura inválida",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(name=request.data['name']).exclude(pk=pk):
            return Response({
                "code": "repeated_name",
                "detailed": "El nombre ya existe",
            }, status=status.HTTP_409_CONFLICT)
        request.data['rol'] = Rol.objects.filter(
            name=request.data['rol']).first()
        serializer = UserSerializer(data=request.data, partial=True)
        user = User.objects.filter(pk=pk).update(**request.data)

        return Response(status=status.HTTP_200_OK)

    def delete(self, request, pk):
        ''' Deletes selected user '''

        User.objects.filter(pk=pk).delete()
        return Response(status=status.HTTP_200_OK)


class GeneralUserView(APIView):

    def post(self, request):
        ''' Creates a new user '''

        validator = Validator({
            'name': {
                'required': True,
                'type': 'string'
            },
            'rol': {
                'required': True,
                'type': 'string',
                'allowed': ['AUX', 'ADM', 'AUD']
            },
            'active': {
                'required': True,
                'type': 'boolean'
            }
        })
        if not validator.validate(request.data):
            return Response({
                "code": "invalid_body",
                "detailed": "Cuerpo con estructura inválida",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(name=request.data['name']).first():
            return Response({
                "code": "repeated_name",
                "detailed": "El nombre ya existe en el sistema",
            }, status=status.HTTP_409_CONFLICT)

        request.data['rol'] = Rol.objects.filter(
            name=request.data['rol']).first()

        user = User.objects.create(**request.data)

        return Response({
            'inserted': user.pk
        }, status=status.HTTP_201_CREATED)

    def get(self, request):
        ''' Searchs user filtered by name '''

        validator = Validator({
            'name': {
                'required': False,
                'type': 'string'
            }
        })
        if not validator.validate(request.GET):
            return Response({
                "code": "invalid_filtering_params",
                "detailed": "Parámetros de filtrado inválidos",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        if not 'name' in request.GET:
            users = User.objects.all()
            return Response({
                'count': users.count(),
                'data': UserSerializer(users, many=True).data
            })

        users = User.objects.filter(name__icontains=request.GET['name'])
        return Response({
            'count': users.count(),
            'data': UserSerializer(users, many=True).data
        })

from cerberus import Validator
from rest_framework.response import Response
from rest_framework import request, status
from cerberus import Validator
from rest_framework.views import APIView

from ..models import Rol


class SpecificRolView(APIView):

    def post(self, request):
        ''' Creates a new user '''

        validator = Validator({
            'name': {
                'required': True,
                'type': 'string',
                'allowed': ['AUX', 'ADM', 'AUD']
            }
        })

        if not validator.validate(request.data):
            return Response({
                "code": "invalid_body",
                "detailed": "Cuerpo con estructura inválida",
                "data": validator.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        rol = Rol.objects.create(**request.data)

        return Response({
            'inserted': rol.pk
        }, status=status.HTTP_201_CREATED)

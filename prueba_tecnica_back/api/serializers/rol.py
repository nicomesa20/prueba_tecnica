from rest_framework import serializers
from ..models import Rol


class RolSerializer(serializers.ModelSerializer):

    class Meta:
        ''' Defines user serializer fields to be returned '''

        model = Rol
        fields = ['pk', 'name']

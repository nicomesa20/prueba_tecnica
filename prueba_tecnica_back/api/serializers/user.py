from rest_framework import serializers

from ..models import User
from ..serializers.rol import RolSerializer


class UserSerializer(serializers.ModelSerializer):

    rol = RolSerializer(read_only=True)

    class Meta:
        ''' Defines user serializer fields to be returned '''

        model = User
        fields = ['pk', 'name', 'rol', 'active']

    def update(self, instance, validated_data):
        print('instance', instance)
        return super().update(instance, validated_data)

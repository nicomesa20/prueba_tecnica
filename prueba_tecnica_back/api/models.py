from django.db import models

# Create your models here.

ROLES = (
    ('ADM', 'Administrador'),
    ('AUD', 'Auditor'),
    ('AUX', 'Auxiliar'),
)


class Rol(models.Model):
    ''' User model definition '''

    name = models.CharField("Nombre", choices=ROLES, max_length=255)

    class Meta:
        ''' Django admin model name visualization '''
        verbose_name = "Rol"
        verbose_name_plural = "Roles"


class User(models.Model):
    ''' User model definition '''

    name = models.CharField("Nombre", max_length=255)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    active = models.BooleanField("Activo")

    class Meta:
        ''' Django admin model name visualization '''
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

from django.urls import path

from .views.user import SpecificUserApi, GeneralUserView
from .views.rol import SpecificRolView
from .views.scripts import RolScriptView

app_name = 'api'

urlpatterns = [
    path('user', GeneralUserView.as_view()),
    path('user/<int:pk>', SpecificUserApi.as_view()),
    path('rol', SpecificRolView.as_view()),
    path('rol-script', RolScriptView.as_view()),
]
